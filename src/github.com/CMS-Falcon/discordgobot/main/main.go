// This file provides a basic "quick start" example of using the Discordgo
// package to connect to Discord using the New() helper function.
package main

import (
	"database/sql"
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/CMS-Falcon/discordgobot/phrases"
	"github.com/CMS-Falcon/discordgobot/trivia"
	"github.com/bwmarrin/discordgo"
)

var db *sql.DB

func main() {

	//seed the rng
	rand.Seed(time.Now().UnixNano())

	// Call the helper function New() passing username and password command
	// line arguments. This returns a new Discord session, authenticates,
	// connects to the Discord data websocket, and listens for events.
	dg, err := discordgo.New(os.Args[1])
	if err != nil {
		fmt.Println(err)
		return
	}

	// Register messageCreate as a callback for the messageCreate events.
	dg.AddHandler(messageCreate)

	// Open the websocket and begin listening.
	dg.Open()

	//initialize database connection
	db, err = sql.Open("mysql", "root:root@/discordgobot_db")

	// Simple way to keep program running until any key press.
	var input string
	fmt.Scanln(&input)
	db.Close()
	dg.Close()
	return
}

// This function will be called (due to AddHandler above) every time a new
// message is created on any channel that the autenticated user has access to.
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {

	var add, triv, invite, roll bool

	//put relevant MessageCreate info into variables
	ch_id := m.ChannelID
	content := m.Content
	mentions := m.Mentions
	user := m.Author

	//get the guild ID of the sent message
	msg_channel, _ := s.Channel(ch_id)
	g_id := msg_channel.GuildID

	help := "`Thanks for using falconbot! Here are a list of all current commands that can be used by mentioning @falconbot:`\n\n" +
		"`help:`  list of all commands.\n" +
		"`addphrase <text>:`  add a phrase to the database.\n" +
		"`getphrase:`  get a random phrase from the database.\n" +
		"`trivia <# (win condition)>:`  start a trivia game in your guild.\n" +
		"`roll <# (2-1000)>:`  rolls a random number between 1 and the given number.\n\n" +
		"`If you have any questions, concerns, or bug reports regarding falconbot, please join the development discord server:` https://discord.gg/0sqoz90BWvi3kd7X"

	//all commands are called by mentioning falconbot. check message for mentions to determine
	//if we should execute any commands
	if len(mentions) != 0 && mentions[0].ID == "156471581504176130" {

		content = strings.TrimSpace(strings.TrimPrefix(content, "<@156471581504176130>")) //remove mention to get message content

		//some commands have parameters. check here to see if any of these are called
		add = strings.HasPrefix(content, "addphrase")
		triv = strings.HasPrefix(content, "trivia")
		invite = strings.HasPrefix(content, "acceptinvite")
		roll = strings.HasPrefix(content, "roll")

		//for any command with no parameters
		switch content {
		case "getphrase":
			randPhrase := phrases.GetRandomPhrase(g_id, db)
			s.ChannelMessageSend(ch_id, randPhrase)
		case "help":
			s.ChannelMessageSend(ch_id, help)
		case "leaveguild":
			//for now only I can issue this command. Later will be extended to server admins
			if user.ID == "114497702653329413" {
				s.GuildLeave(g_id)
			}
		case "leaderboard":
			if trivia.IsActive(g_id, ch_id, db) {
				s.ChannelMessageSend(ch_id, trivia.Leaderboard(g_id, db))
			} else {
				s.ChannelMessageSend(ch_id, "There is no active trivia game for this guild.")
			}
		default:
		}
	}

	//adds a phrase to the database
	if add {
		phrase := strings.TrimSpace(strings.TrimPrefix(content, "addphrase"))
		clean := sanitize(phrase) //sanitize input

		//if input is clean, confirm success. if not, respond with error returned from sanitize()
		if strings.Compare(phrase, clean) == 0 {
			if phrases.AddPhrase(phrase, g_id, db) {
				s.ChannelMessageSend(ch_id, "\""+phrase+"\" has been added to the phrase database!")
			} else {
				s.ChannelMessageSend(ch_id, "There was an error adding your phrase to the database.")
			}
		} else {
			s.ChannelMessageSend(ch_id, clean)
		}
	}

	//invites falconbot to a guild
	if invite {
		inv_id := strings.TrimSpace(strings.TrimPrefix(content, "acceptinvite"))
		s.InviteAccept(inv_id)
	}

	//initializes a trivia game if one is not already active
	if triv {
		gameCreated := false

		win := strings.TrimSpace(strings.TrimPrefix(content, "trivia"))
		i, _ := strconv.Atoi(win)

		//checks that win condition is between 1 and 10. if so, try to initialize game
		if i > 10 || i < 1 {
			s.ChannelMessageSend(ch_id, "Win condition must be between 1-10")
		} else {
			gameCreated = trivia.InitGame(s, g_id, ch_id, i, db)
		}

		//if a trivia game is succesfully initialized, begin the game
		if gameCreated {
			s.ChannelMessageSend(ch_id, "*Trivia game has been created. First player to get "+win+" correct answers will be the winner. "+
				"Type `@falconbot leaderboard` to see the standings for the current game.*")
			q := trivia.AskQuestion(g_id, db)
			s.ChannelMessageSend(ch_id, "`"+q+"`")
		}
	}

	//if there is an active trivia game in the guild AND channel the message is sent from,
	//handle all trivia related actions here
	if trivia.IsActive(g_id, ch_id, db) {
		var correct bool
		isAnswer := false

		//first, check to see if the message is an answer to a question. if so, see if it's correct
		switch content {
		case "a", "A", "b", "B", "c", "C", "d", "D":
			correct = trivia.IsCorrect(g_id, content, db)
			isAnswer = true
		}

		if isAnswer {
			//if the answer is correct, update the user's score and other appropriate fields. if not, repeat question
			if correct {
				s.ChannelMessageSend(ch_id, "Good job, "+user.Username+", you are correct!")
				addScore := trivia.AddScore(g_id, ch_id, user.ID, user.Username, db)
				if !addScore {
					trivia.DeleteGame(g_id, db)
					s.ChannelMessageSend(ch_id, "There was an error getting the question. Unfortunately, the current game has been ended."+
						"If you would like, please join the falconbot development server to report this bug: https://discord.gg/0sqoz90BWvi3kd7X")
				}

				winner := trivia.CheckWin(g_id, user.ID, db)
				//update lastmove on active game
				if winner {
					s.ChannelMessageSend(ch_id, "Congratulations to "+user.Username+"! You are the trivia champion!")
					trivia.DeleteGame(g_id, db)
				} else {
					updateMove := trivia.UpdateLastMove(g_id, db)
					if !updateMove {
						trivia.DeleteGame(g_id, db)
						s.ChannelMessageSend(ch_id, "There was a problem updating the game state. Unfortunately, the current game has been ended."+
							"If you would like, please join the falconbot development server to report this bug: https://discord.gg/0sqoz90BWvi3kd7X")
					}
					//update used questions list
					updateUsed := trivia.UpdateUsed(g_id, db)
					if !updateUsed {
						trivia.DeleteGame(g_id, db)
						s.ChannelMessageSend(ch_id, "There was a problem updating the game state. Unfortunately, the current game has been ended."+
							"If you would like, please join the falconbot development server to report this bug: https://discord.gg/0sqoz90BWvi3kd7X")
					}
					//ask next question
					q := trivia.AskQuestion(g_id, db)
					s.ChannelMessageSend(ch_id, "`"+q+"`")
				}
			} else {
				s.ChannelMessageSend(ch_id, "Sorry, "+user.Username+", that is not the correct answer.")
				q_id := trivia.GetCurrentQuestion(g_id, db)
				s.ChannelMessageSend(ch_id, trivia.RepeatQuestion(q_id, g_id, db))
			}
		}
	}

	//rolls a random number between 1 and user specified value (2 - 1000)
	if roll {
		number := strings.TrimSpace(strings.TrimPrefix(content, "roll"))

		num, err := strconv.Atoi(number)
		if err != nil {
			fmt.Println(err.Error())
		}

		if err != nil || num < 2 || num > 1000 {
			s.ChannelMessageSend(ch_id, "Max roll value must be a number from 2 - 1000.")
		} else {
			roll := random(1, num)
			r := strconv.Itoa(roll)
			s.ChannelMessageSend(ch_id, r)
		}
	}
}

//get random number
func random(min, max int) int {
	return rand.Intn((max+1)-min) + min
}

//rules for phrase input
func sanitize(input string) string {
	if strings.HasPrefix(input, "<@156471581504176130>") {
		return "Invalid phrase. Must not start with \"@falconbot\"."
	}

	if len([]rune(input)) > 255 {
		return "Invalid phrase. Must be under 255 characters."
	}

	if input == "" {
		return "Invalid phrase. Must not be empty string."
	}

	return input
}
