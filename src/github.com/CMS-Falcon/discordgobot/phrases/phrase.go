package phrases

import "fmt"
import _ "github.com/go-sql-driver/mysql"
import "database/sql"

/**
 * @description - adds a user phrase to the db
 * @params - input(string): user phrase to be added
 *         	 guild(string): guild the phrase was sent from (phrases are guild specific)
 *         	 db(*sql.DB): SQL database connection instance
 * @return bool - success or failure
 */
func AddPhrase(input string, guild string, db *sql.DB) bool {

	success := true

	stmtIns, err := db.Prepare("INSERT INTO phrases (phrase, guild) VALUES(?, ?)")
	if err != nil {
		fmt.Printf(err.Error())
		success = false
	}
	defer stmtIns.Close()

	_, err = stmtIns.Exec(input, guild) // Insert phrase
	if err != nil {
		fmt.Printf(err.Error())
		success = false
	}

	return success
}

/**
 * @description - retrieves a random phrase from the guild the request is sent from
 * @params - guild(string): guild of user requesting the phrase
 *         	 db(*sql.DB): SQL database connection instance
 * @return string - phrase from database or error message
 */
func GetRandomPhrase(guild string, db *sql.DB) string {

	stmtOut, err := db.Prepare("SELECT phrase FROM phrases WHERE guild = ? ORDER BY RAND() LIMIT 1")
	if err != nil {
		fmt.Printf(err.Error())
		return "Error retrieving phrase."
	}
	defer stmtOut.Close()

	var result string

	err = stmtOut.QueryRow(guild).Scan(&result)
	if err != nil {
		fmt.Printf(err.Error())
		return "No phrases found."
	}

	return result
}
