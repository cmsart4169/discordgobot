package trivia

import (
	"database/sql"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
)

var fatalError = "There was an error getting the question. Unfortunately, the current game has been ended." +
	"If you would like, please join the falconbot development server to report this bug: https://discord.gg/0sqoz90BWvi3kd7X"

/**
 * @description - initiates a trivia game if one does not already exist
 * @params - s(*discordgo.Session): discordgo Session used for printing messages
 *         	 g_id(string): guild the game was started from
 *         	 ch_id(string): channel the game was started from
 *         	 win(int): win condition for the trivia game
 *         	 db(*sql.DB): SQL database connection instance
 * @return bool - success or failure
 */
func InitGame(s *discordgo.Session, g_id, ch_id string, win int, db *sql.DB) bool {

	var lastmove int64
	currTime := time.Now().Unix()

	stmtCheck, err := db.Prepare("SELECT lastmove FROM active_trivia WHERE guild = ?")
	err = stmtCheck.QueryRow(g_id).Scan(&lastmove)
	if err != nil && err != sql.ErrNoRows {
		fmt.Printf(err.Error())
		s.ChannelMessageSend(ch_id, "There was a problem creating your trivia game. Please try again. "+
			"If problem persists, please join the falconbot development server to report this bug: https://discord.gg/0sqoz90BWvi3kd7X")
		return false
	}
	defer stmtCheck.Close()

	//check to see if game has been active for less than 30 minutes
	//do not allow user to create another game and return false
	//if it has been over 30 minutes, delete game from db and start new one
	timeSince := currTime - lastmove

	if (timeSince) <= 1800 {
		timeleft := (1800 - (timeSince)) / 60
		s.ChannelMessageSend(ch_id, fmt.Sprintf("There is already an active trivia game in this guild. If no moves are made in %v minutes, you may start a new game.", timeleft))
		return false
	} else {
		DeleteGame(g_id, db)
	}

	//insert the new trivia game into active_trivia
	stmtIns, err := db.Prepare("INSERT INTO active_trivia (guild, channel, win, lastmove) VALUES(?, ?, ?, ?)")
	if err != nil {
		fmt.Printf(err.Error())
		s.ChannelMessageSend(ch_id, "There was a problem creating your trivia game. Please try again. "+
			"If problem persists, please join the falconbot development server to report this bug: https://discord.gg/0sqoz90BWvi3kd7X")
		return false
	}
	defer stmtIns.Close()

	_, err = stmtIns.Exec(g_id, ch_id, win, currTime)
	if err != nil {
		fmt.Printf(err.Error())
		s.ChannelMessageSend(ch_id, "There was a problem creating your trivia game. Please try again. "+
			"If problem persists, please join the falconbot development server to report this bug: https://discord.gg/0sqoz90BWvi3kd7X")
		return false
	}

	return true
}

/**
 * @description - gets a question from the database and prints it to channel
 * @params - g_id(string): guild of the current game
 *         	 db(*sql.DB): SQL database connection instance
 * @return string - question from the database or error message
 */
func AskQuestion(g_id string, db *sql.DB) string {

	var used, question string
	var questionID int

	//get the list of used quesitons from the active game
	stmtCheck, err := db.Prepare("SELECT used FROM active_trivia WHERE guild = ?")
	err = stmtCheck.QueryRow(g_id).Scan(&used)
	if err != nil {
		fmt.Printf(err.Error())
		DeleteGame(g_id, db)
		return fatalError
	}
	defer stmtCheck.Close()

	//get a random question that has not already been used in the current game
	stmtQuestion, err := db.Prepare("SELECT question, id FROM trivia_questions WHERE id NOT IN (?) ORDER BY RAND() LIMIT 1")
	err = stmtQuestion.QueryRow(used).Scan(&question, &questionID)
	if err != nil {
		fmt.Printf(err.Error())
		DeleteGame(g_id, db)
		return fatalError
	}
	defer stmtQuestion.Close()

	//assign the question being asked to the current question field in the active game
	stmtCurrent, err := db.Prepare("UPDATE active_trivia SET current_question = ? WHERE guild = ?")
	_, err = stmtCurrent.Exec(questionID, g_id) // Insert phrase
	if err != nil {
		fmt.Printf(err.Error())
		DeleteGame(g_id, db)
		return fatalError
	}

	return question
}

/**
 * @description - prints the current question
 * @params - current(int): id of current question
 *         	 db(*sql.DB): SQL database connection instance
 * @return string - question from the database or error message
 */
func RepeatQuestion(current int, g_id string, db *sql.DB) string {

	var question string

	//get the question that was just asked
	stmtQuestion, err := db.Prepare("SELECT question FROM trivia_questions WHERE id = ?")
	err = stmtQuestion.QueryRow(current).Scan(&question)
	if err != nil {
		fmt.Printf(err.Error())
		DeleteGame(g_id, db)
		return fatalError
	}

	return question
}

/**
 * @description - checks if the answer given is correct
 * @params - g_id(string): guild of the current game
 *         	 content(string): answer submitted
 *         	 db(*sql.DB): SQL database connection instance
 * @return bool - answer correct or not
 */
func IsCorrect(g_id, content string, db *sql.DB) bool {

	var answer string

	//get the id of the current question
	q_id := GetCurrentQuestion(g_id, db)

	//get the answer of the current question of the active game
	stmtAnswer, err := db.Prepare("SELECT answer FROM trivia_questions WHERE id = ?")
	err = stmtAnswer.QueryRow(q_id).Scan(&answer)
	if err != nil {
		fmt.Printf(err.Error())
		return false
	}
	defer stmtAnswer.Close()

	if strings.EqualFold(content, answer) {
		return true
	} else {
		return false
	}

}

/**
 * @description - checks if there is an active trivia game for a guild
 * @params - g_id(string): guild of the current game
 *         	 ch_id(string): channel of the current game
 *         	 db(*sql.DB): SQL database connection instance
 * @return bool - true if active game in guild, false if not
 */
func IsActive(g_id, ch_id string, db *sql.DB) bool {

	stmtCheck, err := db.Prepare("SELECT guild FROM active_trivia WHERE guild = ? AND channel = ?")
	_, err = stmtCheck.Query(g_id, ch_id)
	if err != nil {
		fmt.Printf(err.Error())
		return false
	}
	defer stmtCheck.Close()

	return true
}

/**
 * @description - increments score or adds user if no previous correct answers
 * @params - g_id(string): guild of the current game
 *         	 ch_id(string): channel of the current game
 *         	 u_id(string): ID of user who answered question
 *         	 username(string): username of user who answered question
 *         	 db(*sql.DB): SQL database connection instance
 * @return bool - success or failure
 */
func AddScore(g_id, ch_id, u_id, username string, db *sql.DB) bool {

	var instance, score int
	var exists bool
	instance = GetInstance(g_id, db)

	//check to see if user is currently in active game
	stmtCheck, err := db.Prepare("SELECT EXISTS(SELECT 1 FROM trivia WHERE playerid = ? AND instance = ?)")
	if err != nil {
		fmt.Print(err.Error())
		return false
	}
	err = stmtCheck.QueryRow(u_id, instance).Scan(&exists)
	if !exists {
		addPlayer := AddPlayer(u_id, username, instance, db) //if they aren't, add them to the game
		if !addPlayer {
			return false
		}
		return true
	} else {
		score = GetScore(g_id, u_id, db)
		//if they are, increment their score
		score += 1
		stmtUpdateScore, err := db.Prepare("UPDATE trivia SET score = ? WHERE playerid = ?")
		_, err = stmtUpdateScore.Exec(score, u_id)
		if err != nil {
			fmt.Printf(err.Error())
			return false
		}
		defer stmtUpdateScore.Close()

		return true
	}
}

/**
 * @description - checks if win condition is met
 * @params - g_id(string): guild of the current game
 *         	 u_id(string): ID of user who answered question
 *         	 db(*sql.DB): SQL database connection instance
 * @return bool - true if win condition is met, false if not
 */
func CheckWin(g_id, u_id string, db *sql.DB) bool {

	win := GetWinCondition(g_id, db)
	score := GetScore(g_id, u_id, db)

	if score == win {
		return true
	}

	return false
}

/**
 * @description - adds a player to a trivia game
 * @params - u_id(string): ID of user who answered question
 *         	 username(string): username of user who answered question
 *         	 db(*sql.DB): SQL database connection instance
 * @return bool - success or failure
 */
func AddPlayer(u_id, username string, instance int, db *sql.DB) bool {
	stmtIns, err := db.Prepare("INSERT INTO trivia (instance, playerid, username, score) VALUES(?, ?, ?, ?)")
	if err != nil {
		fmt.Printf(err.Error())
		return false
	}

	_, err = stmtIns.Exec(instance, u_id, username, 1)
	if err != nil {
		fmt.Printf(err.Error())
		return false
	}

	return true
}

/**
 * @description - gets the win condition for a trivia game
 * @params - g_id(string): guild of the current game
 *         	 db(*sql.DB): SQL database connection instance
 * @return int - win condition of current game
 */
func GetWinCondition(g_id string, db *sql.DB) int {

	var win int

	stmtWin, err := db.Prepare("SELECT win FROM active_trivia WHERE guild = ?")
	err = stmtWin.QueryRow(g_id).Scan(&win)
	if err != nil {
		fmt.Printf(err.Error())
	}

	return win
}

/**
 * @description - gets the instance id of the current game
 * @params - g_id(string): guild of the current game
 *         	 db(*sql.DB): SQL database connection instance
 * @return int - current game instance
 */
func GetInstance(g_id string, db *sql.DB) int {

	var instance int

	stmtGetId, err := db.Prepare("SELECT id FROM active_trivia WHERE guild = ?")
	err = stmtGetId.QueryRow(g_id).Scan(&instance)
	if err != nil {
		fmt.Printf(err.Error())
	}

	return instance
}

/**
 * @description - gets the current question of the current trivia game
 * @params - g_id(string): guild of the current game
 *         	 db(*sql.DB): SQL database connection instance
 * @return int - id of current question
 */
func GetCurrentQuestion(g_id string, db *sql.DB) int {

	var q_id int

	stmtQId, err := db.Prepare("SELECT current_question FROM active_trivia WHERE guild = ?")
	err = stmtQId.QueryRow(g_id).Scan(&q_id)
	if err != nil {
		fmt.Printf(err.Error())
	}

	return q_id
}

/**
 * @description - gets the score of a user
 * @params - g_id(string): guild of the current game
 *         	 u_id(string): ID of user to get score
 *         	 db(*sql.DB): SQL database connection instance
 * @return int - score for user
 */
func GetScore(g_id, u_id string, db *sql.DB) int {
	var score int

	instance := GetInstance(g_id, db)

	stmtGetScore, err := db.Prepare("SELECT score FROM trivia WHERE playerid = ? AND instance = ?")
	err = stmtGetScore.QueryRow(u_id, instance).Scan(&score)
	if err != nil {
		fmt.Printf(err.Error())
	}

	return score

}

/**
 * @description - updates list of used questions
 * @params - g_id(string): guild of the current game
 *         	 db(*sql.DB): SQL database connection instance
 * @return bool - success or failure
 */
func UpdateUsed(g_id string, db *sql.DB) bool {

	q_id := GetCurrentQuestion(g_id, db)

	stmtUpdate, err := db.Prepare("UPDATE active_trivia SET used = concat(used, ?, ',') WHERE guild = ?")
	_, err = stmtUpdate.Exec(q_id, g_id)
	if err != nil {
		fmt.Printf(err.Error())
		return false
	}

	return true
}

/**
 * @description - updates time of last move
 * @params - g_id(string): guild of the current game
 *         	 db(*sql.DB): SQL database connection instance
 * @return bool - success or failure
 */
func UpdateLastMove(g_id string, db *sql.DB) bool {

	currTime := time.Now().Unix()

	stmtUpdate, err := db.Prepare("UPDATE active_trivia SET lastmove = ? WHERE guild = ?")
	_, err = stmtUpdate.Exec(currTime, g_id)
	if err != nil {
		fmt.Printf(err.Error())
		return false
	}

	return true
}

/**
 * @description - deletes the current game and its players from the database
 * @params - g_id(string): guild of the current game
 *         	 db(*sql.DB): SQL database connection instance
 * @return
 */
func DeleteGame(g_id string, db *sql.DB) {

	instance := GetInstance(g_id, db)

	stmtDeleteUsers, err := db.Prepare("DELETE FROM trivia WHERE instance = ?")
	_, err = stmtDeleteUsers.Exec(instance)
	if err != nil {
		fmt.Printf(err.Error())
	}

	stmtDeleteGame, err := db.Prepare("DELETE FROM active_trivia WHERE guild = ?")
	_, err = stmtDeleteGame.Exec(g_id)
	if err != nil {
		fmt.Printf(err.Error())
	}
}

/**
 * @description - retrieves all players and constructs a leaderboard message
 * @params - g_id(string): guild of the current game
 *         	 db(*sql.DB): SQL database connection instance
 * @return string - leaderboard message
 */
func Leaderboard(g_id string, db *sql.DB) string {

	var pos, s string
	var instance int

	instance = GetInstance(g_id, db)
	standings := "`Here are the current standings for this guild's game:\n"

	stmtLeader, err := db.Prepare("SELECT username, score FROM trivia WHERE instance = ?")
	rows, err := stmtLeader.Query(instance)
	if err == sql.ErrNoRows {
		return "No players currently have any points."
	}

	i := 1
	for rows.Next() {
		var user string
		var score int
		err = rows.Scan(&user, &score)
		if err != nil {
			fmt.Printf(err.Error())
		}

		pos = strconv.Itoa(i)
		s = strconv.Itoa(score)

		standings += "\n" + pos + ") " + user + " - " + s

		i += 1
	}

	pos = strconv.Itoa(i)
	standings += "\n" + pos + ") Everyone else - 0`"
	return standings
}
